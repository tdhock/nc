\name{stop_for_subject}
\alias{stop_for_subject}
\title{stop for subject}
\description{Error if \code{subject.vec} or \code{pattern} incorrect type.}
\usage{stop_for_subject(subject.vec, 
    pattern)}
\arguments{
  \item{subject.vec}{
}
  \item{pattern}{
}
}



\author{Toby Dylan Hocking}





